<?php

/**
 * @file
 * Provides a PostcodeAnywhere based address field lookup service.
 */

/**
 * Implements hook_menu().
 */
function addressfield_lookup_postcodeanywhere_menu() {
  $items = array();

  $items['admin/config/regional/addressfield-lookup/postcodeanywhere/configure'] = array(
    'title' => 'Postcode Anywhere Settings',
    'description' => 'Configure postcode anywhere.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('addressfield_lookup_postcodeanywhere_config_form'),
    'access arguments' => array('administer addressfield lookup services'),
    'file' => 'includes/addressfield_lookup_postcodeanywhere.admin.inc',
    'weight' => -10,
  );

  return $items;
}

/**
 * Implements hook_addressfield_lookup_postcodeanywhere().
 */
function addressfield_lookup_postcodeanywhere_addressfield_lookup_service_info() {
  return array(
    'postcodeanywhere' => array(
      'name' => t('Postcode Anywhere'),
      'class' => 'AddressFieldLookupPostcodeAnywhere',
      'description' => t('Provides an address field lookup service based on integration with the Postcode Anywhere API.'),
      'config path' => 'admin/config/regional/addressfield-lookup/postcodeanywhere/configure',
      'test data' => 'LL11 5HJ',
    ),
  );
}

/**
 * Implements hook_addressfield_lookup_get_service_object().
 */
function addressfield_lookup_postcodeanywhere_addressfield_lookup_get_service_object($class) {
  global $language;

  $postcode_anywhere = &drupal_static(__FUNCTION__);

  // Check the postcode anywhere configuration variables exist.
  if (!variable_get('addressfield_lookup_postcodeanywhere_license', NULL)) {
    throw new Exception('Postcode Anywhere has not been configured.');
  }

  // Instantiate the API class.
  if (!isset($postcode_anywhere)) {
    $postcode_anywhere = new $class(variable_get('addressfield_lookup_postcodeanywhere_license', NULL), variable_get('addressfield_lookup_postcodeanywhere_login', NULL), $language->name);
  }

  return $postcode_anywhere;
}

/**
 * Implements hook_addressfield_lookup_format_update().
 */
function addressfield_lookup_postcodeanywhere_addressfield_lookup_format_update($format, $address) {
  // Add new GB address elements if the address country is GB.
  if ($address['country'] == 'GB') {
    // Add the 'sub premise' element if it is not defined.
    if (empty($format['street_block']['sub_premise'])) {
      $format['street_block']['sub_premise'] = array(
        '#title' => t('Flat No.'),
        '#tag' => 'div',
        '#attributes' => array('class' => array('sub-premise')),
        '#size' => 30,
        '#weight' => -10,
      );

      // Re-order the form elements considering 'sub premise' element.
      $format['street_block']['premise']['#weight'] = -9;
    }

    // Add the 'dependent locality' element if it is not defined.
    if (empty($format['locality_block']['dependent_locality'])) {
      $format['locality_block']['dependent_locality'] = array(
        '#title' => t('Address 2'),
        '#tag' => 'div',
        '#attributes' => array('class' => array('dependent-locality')),
        '#size' => 30,
      );
    }

    // Rename the form elements considering 'dependent locality' element.
    $format['street_block']['premise']['#title'] = t('House Name/Building');
  }
  else {
    // This is not a GB address.
    // Remove all address field lookup elements.
    foreach ($format as $element_name => $element) {
      if (stristr($element_name, 'addressfield_lookup')) {
        unset($format[$element_name]);
      }
    }

    // Reset the address field lookup mode.
    unset($address['addressfield_lookup_mode']);

    // Ensure all normal address elements are visible.
    $format['street_block']['#access'] = TRUE;
    $format['locality_block']['#access'] = TRUE;
    $format['name_block']['#access'] = TRUE;
    $format['organisation_block']['#access'] = TRUE;
  }

  return $format;
}
